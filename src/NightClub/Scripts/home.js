﻿$(document).ready(function () {
    
    //КОНСТАНТЫ
    var constants = {
        $templateSelector: $("#template"),
        visitors: "#visitors",
        template : null,
        $visitorsWrap: $("#visitorsWrap"),
        buttons: ".music-btn"
    };


    //ФУНКЦИИ
    var functions = {
        fillTable: function () {
            debugger;
            $.ajax({
                type: "POST",
                url: "/Home/GetNightClub",
                data: "",
                success: function(result) {
                    if (result.ResponseStatus.Code == 200) {
                        
                        functions.unbindHandlers();
                        $(constants.visitors).remove();
                        $("<div id='visitors'></div>").appendTo(constants.$visitorsWrap);
                        functions.bindHandlers();
                        $(constants.visitors).html(constants.template(result));
                    } else {
                        alert(result.ResponseStatus.Error);
                    }
                }
            });
        },

        registerHelpers: function() {
            Handlebars.registerHelper('musicIs', function(music, checkedNum) {
                if (music == checkedNum)
                    return 'active';
                else return '';
            });
        },

        init: function () {
            constants.template = Handlebars.compile(constants.$templateSelector.html());
            functions.registerHelpers();
            functions.fillTable();
        },

        bindHandlers: function () {
            $(document).on('click', ".music-btn", eventHandlers.onMusicBtnClick);
        },
        
        unbindHandlers: function () {
            $(document).unbind('click');
        },
    };

    //Обработчики событий
    var eventHandlers = {
        onMusicBtnClick: function (event) {
            var btn = event.target || event.srcElement;
            var musicType = $(btn).data('music');

            $.ajax({
                type: "POST",
                url: "/Home/ChangeMusic",
                data: { music: musicType },
                success: function(result) {
                    if (result.ResponseStatus.Code == 200) {
                        functions.fillTable();
                    } else {
                        alert(result.ResponseStatus.Error);
                    }
                }
            });
        }
    };

    functions.init();
});