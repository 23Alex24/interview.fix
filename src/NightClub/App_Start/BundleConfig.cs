﻿using System.Web.Optimization;

namespace NightClub.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/scripts/main").Include(
                "~/Scripts/jquery/jquery-2.1.1.min.js",
                "~/Scripts/bootstrap/bootstrap.min.js",
                "~/Scripts/handlebars-v1.3.0.js",
                "~/Scripts/home.js"));

            bundles.Add(new StyleBundle("~/bundles/styles/main").Include(
                "~/Content/Styles/bootstrap/bootstrap.min.css",
                "~/Content/Styles/main.css"));
        }
    }
}