﻿using BLL.Enums;

namespace BLL.Entities
{
    public interface IBodyPart
    {
        BodyPartType BodyPartType { get; set; }

        object GetState();
    }
}
