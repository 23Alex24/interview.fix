﻿using BLL.Enums;

namespace BLL.Entities
{
    public class DrinkSkill : ISkill
    {
        public DrinkType DrinkType { get; set; }

        public bool IsRunning { get; set; }

        public string Description { get; set; }

        public SkillType SkillType { get; set; }
    }
}