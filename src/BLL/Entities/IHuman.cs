﻿using BLL.Enums;
using System;
using System.Collections.Generic;

namespace BLL.Entities
{
    public interface IHuman
    {
        Guid Id { get; set; }

        GenderType Gender { get; set; }

        IEnumerable<ISkill> Skills { get; set; }

        IEnumerable<IBodyPart> BodyParts { get; set; }
    }
}
