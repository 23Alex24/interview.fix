﻿using BLL.Enums;

namespace BLL.Entities
{
    public class DanceSkill : ISkill
    {
        public MusicType MusicType { get; set; }

        /// <summary>Выполняет ли сейчас умение человек</summary>
        public bool IsRunning { get; set; }

        public string Description { get; set; }

        public SkillType SkillType { get; set; }
    }
}