﻿using System.Collections.Generic;
using BLL.Enums;

namespace BLL.Entities
{
    public interface INightClub
    {
        IEnumerable<IHuman> Visitors { get; set; }

        MusicType ActiveMusic { get; set; }
    }
}
