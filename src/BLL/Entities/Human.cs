﻿using BLL.Enums;
using System;
using System.Collections.Generic;

namespace BLL.Entities
{
    public class Human : IHuman
    {
        public Guid Id { get; set; }

        public GenderType Gender { get; set; }

        /// <summary>Набор умений, которые человек может выполнять</summary>
        public IEnumerable<ISkill> Skills { get; set; }

        /// <summary>Части тела человека</summary>
        public IEnumerable<IBodyPart> BodyParts { get; set; }
    }
}