﻿using System;
using System.Collections.Generic;
using BLL.Enums;

namespace BLL.ModelBuilders
{
    public class MusicListModelBuilder
    {
        public Dictionary<string, int> Build()
        {
            var model = new Dictionary<string, int>();

            foreach (var m in Enum.GetValues(typeof(MusicType)))
            {
                model.Add(Enum.GetName(typeof (MusicType), m), (int) m);
            }

            return model;
        }
    }
}