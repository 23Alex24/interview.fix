﻿using System.Collections.Generic;
using System.Net;
using BLL.Entities;
using BLL.Enums;
using BLL.Models;
using BLL.Extensions;
using System.Linq;

namespace BLL.ModelBuilders
{
    public class NightClubModelBuilder
    {
        public NightClubModel Build(INightClub club)
        {
            var model = new NightClubModel
                {
                    ActiveMusic = club.ActiveMusic,
                    ActiveMusicName = club.ActiveMusic.GetDescription(),
                    Visitors = new List<HumanModel>(),
                    ResponseStatus = new ResponseStatusModel()
                };
            var visitors = (List<IHuman>)club.Visitors;

            foreach (var visitor in visitors)
            {
                var human = visitor;
                var runningSkill = visitor.Skills.SingleOrDefault(s => s.IsRunning);
                var CanDanceMusic = (DanceSkill) human.Skills.First(s => s.SkillType == SkillType.Dance);

                model.Visitors.Add(new HumanModel
                    {
                        Gender = visitor.Gender.GetDescription(),
                        Id = visitor.Id,
                        RunningSkill = runningSkill == null ? "" : runningSkill.Description,
                        HandsState = human.GetBodyPartStateDescription(BodyPartType.Hands),
                        HeadState = human.GetBodyPartStateDescription(BodyPartType.Head),
                        LegsState = human.GetBodyPartStateDescription(BodyPartType.Legs),
                        TorsoState = human.GetBodyPartStateDescription(BodyPartType.Torso),
                        CanDanceMusic = CanDanceMusic.MusicType.GetDescription()
                    });

                model.ResponseStatus.Code = HttpStatusCode.OK;
            }

            return model;
        }
    }
}