﻿using System.ComponentModel;
using BLL.Constants;

namespace BLL.Enums
{
    public enum MusicType
    {
        [Description(StringConstants.Descriptions.Rnb)]
        Rnb = 0,

        [Description(StringConstants.Descriptions.Electrohouse)]
        Electrohouse = 1,

        [Description(StringConstants.Descriptions.Pop)]
        Pop = 2
    }
}