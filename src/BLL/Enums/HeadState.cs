﻿using System.ComponentModel;
using BLL.Constants;

namespace BLL.Enums
{
    public enum HeadState : byte
    {
        None = 0,

        [Description(StringConstants.Descriptions.DrinkVodka)]
        DrinkVodka = 1,

        /// <summary>Покачивание назад вперед</summary>
        [Description(StringConstants.Descriptions.BackForwardSwinging)]
        BackForwardSwinging = 2,

        /// <summary>Плавные движения</summary>
        [Description(StringConstants.Descriptions.SmoothMovements)]
        SmoothMovements = 3,

        /// <summary>Небольшие движения</summary>
        [Description(StringConstants.Descriptions.SmallMovements)]
        SmallMovements = 4
    }
}