﻿using System.ComponentModel;
using BLL.Constants;

namespace BLL.Enums
{
    public enum LegsState : byte
    {
        None =0,

        [Description(StringConstants.Descriptions.DrinkVodka)]
        DrinkVodka =1,

        /// <summary>Полуприсед</summary>
        [Description(StringConstants.Descriptions.Crouch)]
        Crouch =2,

        /// <summary>Плавные движения</summary>
        [Description(StringConstants.Descriptions.SmoothMovements)]
        SmoothMovements = 3,

        /// <summary>Небольшие движения</summary>
        [Description(StringConstants.Descriptions.SmallMovements)]
        SmallMovements = 4,

        /// <summary>Движения в ритме</summary>
        [Description(StringConstants.Descriptions.RhythmMovenment)]
        RhythmMovenment = 5
    }
}