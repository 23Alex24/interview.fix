﻿using BLL.Entities;
using BLL.Enums;

namespace BLL.Services
{
    public interface IHumanService
    {
        void DanceOrDrink(IHuman human, MusicType music);
    }
}
