﻿using BLL.Entities;
using BLL.Enums;
using System.Linq;

namespace BLL.Services
{
    public class HumanService : IHumanService
    {
        public void DanceOrDrink(IHuman human, MusicType music)
        {
            foreach (var s in human.Skills)
            {
                s.IsRunning = false;
            }

            var danceSkils = human.Skills.Where(s => s.SkillType == SkillType.Dance).ToList();
            var skill = danceSkils.FirstOrDefault(s => ((DanceSkill)s).MusicType == music);            

            if (skill != null)
            {
                switch (music)
                {
                    case MusicType.Electrohouse:
                        ChangeBodyPartsState(human, TorsoState.BackForwardSwinging, HeadState.SmallMovements,
                                             HandsState.Rotation, LegsState.RhythmMovenment);
                        break;

                    case MusicType.Rnb:
                        ChangeBodyPartsState(human, TorsoState.BackForwardSwinging, HeadState.BackForwardSwinging,
                                             HandsState.BentElbows, LegsState.Crouch);
                        break;

                    default:
                        ChangeBodyPartsState(human, TorsoState.SmoothMovements, HeadState.SmoothMovements,
                                             HandsState.SmoothMovements, LegsState.SmoothMovements);
                        break;
                }

                skill.IsRunning = true;
            }
            else
            {
                ChangeBodyPartsState(human, TorsoState.DrinkVodka, HeadState.DrinkVodka,
                                            HandsState.DrinkVodka, LegsState.DrinkVodka);
                var drinkSkill = human.Skills.First(s => s.SkillType == SkillType.Drink);
                drinkSkill.IsRunning = true;
            }
        }

        private void ChangeBodyPartsState(IHuman human, TorsoState torsoState, HeadState headState,
            HandsState handsState, LegsState legsState)
        {
            var man = (Human) human;
            var torso = (BodyPart<TorsoState>) man.BodyParts.First(p => p.BodyPartType == BodyPartType.Torso);
            var head = (BodyPart<HeadState>) man.BodyParts.First(p => p.BodyPartType == BodyPartType.Head);
            var hands = (BodyPart<HandsState>)man.BodyParts.First(p => p.BodyPartType == BodyPartType.Hands);
            var legs = (BodyPart<LegsState>)man.BodyParts.First(p => p.BodyPartType == BodyPartType.Legs);
            torso.State = torsoState;
            head.State = headState;
            hands.State = handsState;
            legs.State = legsState;
        }
    }
}