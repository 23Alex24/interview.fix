﻿using System.Collections.Generic;
using System.Threading;
using BLL.Constants;
using BLL.Entities;
using BLL.Enums;
using BLL.Factories;
using BLL.Other;

namespace BLL.Services
{
    public class NightClubService : INightClubService
    {
        private readonly IHumanService _humanService;
        private readonly HumanFactoryBase _humanFactory;

        public NightClubService(HumanFactoryBase humanFactory, IHumanService humanService)
        {
            _humanService = humanService;
            _humanFactory = humanFactory;
        }



        public INightClub GetNightClub()
        {
            var nightClub = CacheManager.GetNightClub();

            if (nightClub == null)
            {
                var club = CreateNightClub();
                CacheManager.SetNightClub(club);
                ChangeMusic(club.ActiveMusic);
                return club;
            }

            return nightClub;
        }

        public void ChangeMusic(MusicType musicType)
        {
            var club = GetNightClub();
            club.ActiveMusic = musicType;
            foreach (var human in club.Visitors)
            {
                _humanService.DanceOrDrink(human,musicType);
            }
            CacheManager.SetNightClub(club);
        }

        private INightClub CreateNightClub()
        {
            var club = new BLL.Entities.NightClub();

            var humans = new List<IHuman>();

            for (int i = 0; i < NumericConstants.ClubVisitorsCount; i++)
            {
                humans.Add(_humanFactory.CreateRandomHuman());
                //Для рандомности
                Thread.Sleep(27);
            }

            club.Visitors = humans;
            club.ActiveMusic = MusicType.Electrohouse;
            return club;
        }
    }
}