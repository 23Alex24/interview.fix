﻿using BLL.Entities;
using BLL.Enums;
using BLL.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace BLL.Constants
{
    public static class EntitiesConstants
    {
        public static readonly ReadOnlyCollection<ISkill> DanceSkills;

        static EntitiesConstants()
        {
            var skills = new List<ISkill>();

            foreach (var musicType in Enum.GetValues(typeof (MusicType)))
            {
                var music = (MusicType) musicType;
                var description = string.Format("{0} {1}", "Танцует", music.GetDescription());

                skills.Add(new DanceSkill
                    {
                        MusicType = music, 
                        Description = description,
                        SkillType = SkillType.Dance
                    });
            }

            DanceSkills = new ReadOnlyCollection<ISkill>(skills);            
        }
    }
}