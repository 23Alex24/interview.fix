﻿namespace BLL.Constants
{
    public class NumericConstants
    {
        /// <summary>Какое кол-во умений танцевать создавать рандомно для человека</summary>
        public const int RandomDanceSkillsCount = 1;

        /// <summary>Сколько создавать людей для клуба</summary>
        public const int ClubVisitorsCount = 10;
    }
}