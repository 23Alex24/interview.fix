﻿using BLL.Entities;
using BLL.Enums;
using System;
using System.Collections.Generic;

namespace BLL.Factories
{
    public sealed class DanceSkillFactory : SkillFactoryBase
    {
        public override ISkill CreateRandomSkill(IEnumerable<ISkill> skills)
        {
            var skillsList = new List<ISkill>(skills);

            if (skillsList == null || skillsList.Count < 1)
            {
                throw new ArgumentNullException("skills");
            }

            var r = new Random();
            var i = r.Next(0, skillsList.Count);
            var randomSkill = (DanceSkill) skillsList[i];

            return new DanceSkill
                {
                    MusicType = randomSkill.MusicType, 
                    Description = randomSkill.Description,
                    SkillType = SkillType.Dance
                };
        }        
    }
}