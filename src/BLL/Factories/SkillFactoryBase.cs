﻿using BLL.Entities;
using System.Collections.Generic;

namespace BLL.Factories
{
    public abstract class SkillFactoryBase : ISkillFactory
    {
        public abstract ISkill CreateRandomSkill(IEnumerable<ISkill> skills);
    }
}