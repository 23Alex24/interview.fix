﻿using BLL.Entities;
using BLL.Enums;
using System;
using System.Linq;

namespace BLL.Extensions
{
    public static class HumanExtensions
    {
        public static string GetBodyPartStateDescription(this IHuman human, BodyPartType type)
        {
            var part = human.BodyParts.Single(p => p.BodyPartType == type);
            var result = ((Enum) part.GetState()).GetDescription();
            return result;
        }
    }
}