# Тестовое задание для Fix

Выполнял в июле 2014. 

#### Документация

* [Описание тестового задания тут](https://gitlab.com/23Alex24/interview.fix/blob/master/docs/home.md) и в файле **Тестовое задание.docx** в корне проекта.
* [Скриншоты ](https://gitlab.com/23Alex24/interview.fix/blob/master/docs/screenshots.md)
* [Что можно улучшить, что сделано плохо](https://gitlab.com/23Alex24/interview.fix/blob/master/docs/analysis.md)

#### Запуск

* База данных не используется
* Запускать в VS 2015 или VS 2017

#### Стек технологий

* .NET Framework 4.5, C#
* Html, Bootstrap, JS, Handlebars.js